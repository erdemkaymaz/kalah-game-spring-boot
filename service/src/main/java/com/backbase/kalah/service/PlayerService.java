package com.backbase.kalah.service;

import com.backbase.kalah.model.Player;
import com.backbase.kalah.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    public Player getPlayerById(Long id) {
        return playerRepository.findOne(id);
    }

}
