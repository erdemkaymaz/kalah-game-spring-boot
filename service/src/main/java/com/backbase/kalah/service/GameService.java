package com.backbase.kalah.service;

import com.backbase.kalah.model.KalahGame;
import com.backbase.kalah.repository.KalahGameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameService {

    @Autowired
    private KalahGameRepository kalahGameRepository;

    public KalahGame getKalahGameById(Long id) {
        return kalahGameRepository.findOne(id);
    }

    public KalahGame addKalahGame(KalahGame game) {
        return kalahGameRepository.save(game);
    }

    public void dropGame(Long gameId) {
        kalahGameRepository.delete(gameId);
    }

}
