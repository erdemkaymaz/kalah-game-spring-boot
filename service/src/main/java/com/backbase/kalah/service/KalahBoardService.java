package com.backbase.kalah.service;

import com.backbase.kalah.model.KalahBoard;
import com.backbase.kalah.model.KalahStore;
import com.backbase.kalah.model.Pit;
import com.backbase.kalah.model.Player;
import com.backbase.kalah.repository.KalahBoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KalahBoardService {

    @Autowired
    private KalahBoardRepository kalahBoardRepository;

    public KalahBoard getKalahBoardById(Long id) {
        return kalahBoardRepository.findOne(id);
    }

    public KalahBoard prepareKalahBoard(Player[] players) {

        Player player1 = players[0];
        Player player2 = players[1];

        KalahBoard board = new KalahBoard();

        for (int i = 1; i <= 6; i++) {
            Pit pit = new Pit();
            pit.setNumberOfStones(6);
            pit.setPitOwner(player1);
            pit.setPitNumber(i);

            Pit pit2 = new Pit();
            pit2.setNumberOfStones(6);
            pit2.setPitOwner(player2);
            pit2.setPitNumber(i);

            board.getPits().add(pit);
            board.getPits().add(pit2);
        }

        KalahStore kalahStoreForPlayer1 = new KalahStore();
        KalahStore kalahStoreForPlayer2 = new KalahStore();

        kalahStoreForPlayer1.setKalahStoreNumber(1);
        kalahStoreForPlayer2.setKalahStoreNumber(2);

        kalahStoreForPlayer1.setKalahStoreOwner(player1);
        kalahStoreForPlayer2.setKalahStoreOwner(player2);

        board.getKalahStores().add(kalahStoreForPlayer1);
        board.getKalahStores().add(kalahStoreForPlayer2);

        return board;

    }

    public void play(KalahBoard board, Integer pitnum, Integer numStones, Player player) {

        int takeStonesFromPit = 0;

        if (player.getPlayerNumber().equals(1)) { // Player 1 Move

            takeStonesFromPit = board.getPits().get(pitnum - 1).getNumberOfStones();
        } else {
            takeStonesFromPit = board.getPits().get(pitnum + 5).getNumberOfStones();
        }

        if (player.getPlayerNumber().equals(1)) {
            Integer stonesOfPit = board.getPits().get(pitnum - 1).getNumberOfStones();

            int remainedMoves = stonesOfPit;

            for (int i = 1; i <= stonesOfPit; i++, remainedMoves--) {

                if (board.getPits().get(pitnum - 1).getPitOwner().getPlayerNumber() == 1) {

                    int nextPitIndex = pitnum - 1 + i;
                    int lastPitIndex = nextPitIndex;

                    if (nextPitIndex == 6) {
                        board.getKalahStores().get(0).setNumberOfStones(
                                (Integer) (board.getKalahStores().get(0).getNumberOfStones() + 1)
                        );

                        if (i + 1 > stonesOfPit) {
                            break;
                        } else {
                            --stonesOfPit;
                        }

                    }

                    if (nextPitIndex <= 11) {
                        board.getPits().get(nextPitIndex)
                                .setNumberOfStones(
                                        (Integer) (board.getPits().get(nextPitIndex).getNumberOfStones() + 1)
                                );
                    } else {
                        int cyclicPitIndex = nextPitIndex - 12;

                        while (cyclicPitIndex >= 12) {
                            cyclicPitIndex -= 12;
                        }

                        lastPitIndex = cyclicPitIndex;

                        if (cyclicPitIndex == 6) {
                            board.getKalahStores().get(0).setNumberOfStones(
                                    (Integer) (board.getKalahStores().get(0).getNumberOfStones() + 1)
                            );

                            if (i + 1 > stonesOfPit) {
                                break;
                            } else {
                                --stonesOfPit;
                            }
                        }

                        board.getPits().get(cyclicPitIndex)
                                .setNumberOfStones(
                                        (Integer) (board.getPits().get(cyclicPitIndex).getNumberOfStones() + 1)
                                );
                    }

                    if (i == stonesOfPit) {

                        int numOfStonesInLastPit = board.getPits().get(lastPitIndex).getNumberOfStones();

                        if (numOfStonesInLastPit == 1
                                && board.getPits().get(lastPitIndex).getPitOwner().getPlayerNumber() == 1) {

                            int KalahCurrentNumOfStones = board.getKalahStores().get(0).getNumberOfStones();
                            int oppsiteSideStones = board.getPits()
                                    .get(lastPitIndex + (11 - (2 * lastPitIndex))).getNumberOfStones();

                            int lastIndexStones = board.getPits().get(lastPitIndex).getNumberOfStones();

                            board.getKalahStores().get(0)
                                    .setNumberOfStones((Integer) (KalahCurrentNumOfStones + oppsiteSideStones + lastIndexStones));

                            board.getPits().get(lastPitIndex).setNumberOfStones((Integer) 0);
                            board.getPits().get(lastPitIndex + (11 - (2 * lastPitIndex))).setNumberOfStones((Integer) 0);
                        }

                        board.getKalahGame().setGameTurn(2);
                    }

                }
            }
            board.getPits().get(pitnum - 1).setNumberOfStones((Integer) (board.getPits().get(pitnum - 1).getNumberOfStones() - takeStonesFromPit));

        } else // Player 2 Move
        {
            Integer stonesOfPit = board.getPits().get(pitnum + 5).getNumberOfStones();

            int remainedMoves = stonesOfPit;

            for (int i = 1; i <= stonesOfPit; i++, remainedMoves--) {
                if (board.getPits().get(pitnum + 5).getPitOwner().getPlayerNumber() == 2) {

                    int nextPitIndex = pitnum + 5 + i;
                    int lastPitIndex = nextPitIndex;

                    if (nextPitIndex <= 11) {
                        board.getPits().get(nextPitIndex)
                                .setNumberOfStones(
                                        (Integer) (board.getPits().get(nextPitIndex).getNumberOfStones() + 1)
                                );
                    } else {

                        int cyclicPitIndex = nextPitIndex - 12;

                        while (cyclicPitIndex >= 12) {
                            cyclicPitIndex -= 12;
                        }

                        lastPitIndex = cyclicPitIndex;

                        if (cyclicPitIndex == 0) {
                            board.getKalahStores().get(1).setNumberOfStones(
                                    (Integer) (board.getKalahStores().get(1).getNumberOfStones() + 1)
                            );

                            if (i + 1 > stonesOfPit) {
                                break;
                            } else {
                                --stonesOfPit;
                            }
                        }

                        board.getPits().get(cyclicPitIndex)
                                .setNumberOfStones(
                                        (Integer) (board.getPits().get(cyclicPitIndex).getNumberOfStones() + 1)
                                );
                    }

                    if (i == stonesOfPit) {

                        int numOfStonesInLastPit = board.getPits().get(lastPitIndex).getNumberOfStones();

                        if (numOfStonesInLastPit == 1
                                && board.getPits().get(lastPitIndex).getPitOwner().getPlayerNumber() == 2) {

                            int KalahCurrentNumOfStones = board.getKalahStores().get(1).getNumberOfStones();
                            int oppsiteSideStones = board.getPits()
                                    .get(lastPitIndex - (11 - (2 * (11 - lastPitIndex)))).getNumberOfStones();

                            int lastIndexStones = board.getPits().get(lastPitIndex).getNumberOfStones();

                            board.getKalahStores().get(1)
                                    .setNumberOfStones((Integer) (KalahCurrentNumOfStones + oppsiteSideStones + lastIndexStones));

                            board.getPits().get(lastPitIndex).setNumberOfStones((Integer) 0);
                            board.getPits().get(lastPitIndex - (11 - (2 * (11 - lastPitIndex)))).setNumberOfStones((Integer) 0);
                        }

                        board.getKalahGame().setGameTurn(1);

                    }

                }
            }

            board.getPits().get(pitnum + 5).setNumberOfStones((Integer) (board.getPits().get(pitnum + 5).getNumberOfStones() - takeStonesFromPit));
        }

        boolean isGameFinished = true;

        for (int i = 0; i <= 5; i++) {

            int stonesNum = board.getPits().get(i).getNumberOfStones();

            if (stonesNum > 0) {
                isGameFinished = false;
                break;
            }

        }

        if (!isGameFinished) {
            isGameFinished = true;
            for (int i = 6; i <= 11; i++) {

                int stonesNum = board.getPits().get(i).getNumberOfStones();

                if (stonesNum > 0) {
                    isGameFinished = false;
                    break;
                }

            }
        }

        if (isGameFinished) {

            board.getKalahGame().setGameStatus("GameOver");
            calculateWinner(board);
            board.getKalahGame().setGameTurn(-1);

        } else {
            board.getKalahGame().setGameStatus("Playing");

        }

        this.kalahBoardRepository.save(board);

    }

    private void calculateWinner(KalahBoard board) {

        int stonesOfPlayer1 = 0;
        int stonesOfPlayer2 = 0;

        for (int i = 0; i <= 5; i++) {

            stonesOfPlayer1 += board.getPits().get(i).getNumberOfStones();

        }

        stonesOfPlayer1 += board.getKalahStores().get(0).getNumberOfStones();

        for (int i = 6; i <= 11; i++) {
            stonesOfPlayer2 += board.getPits().get(i).getNumberOfStones();

        }

        stonesOfPlayer2 += board.getKalahStores().get(1).getNumberOfStones();

        if (stonesOfPlayer1 == stonesOfPlayer2) {
            board.getKalahGame().setGameStatus("draw");
        } else if (stonesOfPlayer1 > stonesOfPlayer2) {
            board.getKalahGame().setGameStatus("Player I Won");
        } else {
            board.getKalahGame().setGameStatus("Player II Won");
        }

    }

    private int sumOfStonesofAllpits(KalahBoard board) {

        int sum = 0;

        for (Pit pit : board.getPits()) {

            sum += pit.getNumberOfStones();

        }

        sum += board.getKalahStores().get(0).getNumberOfStones();
        sum += board.getKalahStores().get(1).getNumberOfStones();

        return sum;
    }

}
