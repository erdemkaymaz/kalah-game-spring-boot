package com.backbase.controller;


import com.backbase.kalah.RestApplication;
import com.backbase.kalah.model.KalahBoard;
import com.backbase.kalah.model.KalahGame;
import com.backbase.kalah.model.Player;
import com.backbase.kalah.service.GameService;
import com.backbase.kalah.service.KalahBoardService;
import com.backbase.kalah.service.PlayerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RestApplication.class)
public class GameControllerTest {

    @MockBean
    private GameService gameService;

    @MockBean
    private PlayerService playerService;

    @MockBean
    private KalahBoardService kalahBoardService;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void createGameTest() throws Exception {

        KalahGame game = new KalahGame();
        game.setId(1l);
        Player player = new Player();
        player.setName("test_name");
        game.getPlayers().add(player);
        given(this.gameService.getKalahGameById(1l)).willReturn(game);
        given(this.playerService.getPlayerById(1l)).willReturn(player);

        this.mockMvc.perform(get("/game/createGame/" + 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("players[0].name", is("test_name")));
    }

    @Test
    public void dropGameTest() throws Exception {
        KalahGame game = new KalahGame();
        game.setId(new Long(1));
        this.gameService.dropGame(new Long(1));
        this.gameService.addKalahGame(game);
        mockMvc.perform(delete("/game/drop/" + 1).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void joinGameTest() throws Exception {
        KalahGame game = new KalahGame();
        game.setId(1l);
        Player player = new Player();
        player.setName("test_name");
        game.getPlayers().add(player);

        Player player2 = new Player();
        player.setName("test_name");

        Player[] players = new Player[2];
        players[0] = player;
        players[1] = player2;

        KalahBoard kalahBoard = new KalahBoard();
        game.setBoard(kalahBoard);

        given(this.gameService.getKalahGameById(new Long(1))).willReturn(game);
        given(this.playerService.getPlayerById(new Long(2))).willReturn(player);
        given(this.kalahBoardService.prepareKalahBoard(players)).willReturn(kalahBoard);

        this.mockMvc.perform(put("/game/join/" + 1 + "/" + 2))
                .andExpect(status().isOk());
    }

    @Test
    public void playGameTest() throws Exception {

        KalahGame game = new KalahGame();
        game.setId(1l);
        Player player = new Player();
        player.setName("test_name");
        game.getPlayers().add(player);

        KalahBoard kalahBoard = new KalahBoard();
        game.setBoard(kalahBoard);

        String content = "{\"playerId\": \"120\" , \"numstones\": \"11\" ," +
                "\"pitnum\": \"1\" , \"gameId\": \"100\"}";

        given(this.gameService.getKalahGameById(new Long(100))).willReturn(game);

        this.mockMvc.perform(post("/game/play").contentType(MediaType.APPLICATION_JSON).characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON).content(content)).andExpect(status().isOk());
    }

}