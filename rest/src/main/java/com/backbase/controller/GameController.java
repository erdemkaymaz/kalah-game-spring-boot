package com.backbase.controller;


import com.backbase.exception.RequestBodyParameterNotFoundException;
import com.backbase.kalah.model.KalahBoard;
import com.backbase.kalah.model.KalahGame;
import com.backbase.kalah.model.Player;
import com.backbase.kalah.service.GameService;
import com.backbase.kalah.service.KalahBoardService;
import com.backbase.kalah.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/game")
public class GameController {

    @Autowired
    private GameService gameService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private KalahBoardService kalahBoardService;

    @RequestMapping(value = "/createGame/{playerId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<KalahGame> createGame(@PathVariable Long playerId) {

        KalahGame game = new KalahGame();
        Player player = this.playerService.getPlayerById(playerId);
        game.getPlayers().add(player);
        this.gameService.addKalahGame(game);
        return new ResponseEntity<KalahGame>(game, HttpStatus.OK);
    }

    @RequestMapping(value = "/drop/{gameId}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity dropGame(@PathVariable Long gameId) {

        this.gameService.dropGame(gameId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/join/{gameId}/{playerId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<KalahGame> joinGame(@PathVariable Long gameId, @PathVariable Long playerId) throws Exception {

        KalahGame game = this.gameService.getKalahGameById(gameId);
        Player player = this.playerService.getPlayerById(playerId);

        if (game.getPlayers().size() == 1) {
            game.getPlayers().add(player);
            KalahBoard board = this.kalahBoardService.prepareKalahBoard(game.getPlayers().toArray(new Player[2]));
            return new ResponseEntity<KalahGame>(game, HttpStatus.OK);
        } else {
            // todo : exception handling
            throw new Exception("Error Occured!!!");
        }
    }


    @RequestMapping(value = "/play", method = RequestMethod.POST)
    public ResponseEntity<KalahGame> play(@RequestBody Map<String, String> params) throws RequestBodyParameterNotFoundException {

        if (!params.containsKey("playerId") || !params.containsKey("numstones")
                || !params.containsKey("pitnum") || !params.containsKey("gameId")) {
            throw new RequestBodyParameterNotFoundException();
        }

        Player player = this.playerService.getPlayerById(new Long(params.get("playerId")));

        KalahGame kalahGame = this.gameService.getKalahGameById(new Long(params.get("gameId")));
        KalahBoard kalahBoard = kalahGame.getBoard();

        this.kalahBoardService.play(kalahBoard, new Integer(params.get("pitnum")), new Integer(params.get("numstones")), player);

        return new ResponseEntity<KalahGame>(kalahGame, HttpStatus.OK);
    }

}
