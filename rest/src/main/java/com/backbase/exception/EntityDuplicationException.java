package com.backbase.exception;

public class EntityDuplicationException extends Exception{
    public EntityDuplicationException(String message) {
        super(message);
    }

    public EntityDuplicationException() {
    }
}
