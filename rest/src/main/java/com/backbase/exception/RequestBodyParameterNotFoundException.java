package com.backbase.exception;

public class RequestBodyParameterNotFoundException extends Exception {

    public RequestBodyParameterNotFoundException(String message) {
        super(message);
    }

    public RequestBodyParameterNotFoundException() {
    }
}