package com.backbase.exception;

public class EntityDeletionException extends Exception {

    public EntityDeletionException(String message) {
        super(message);
    }

    public EntityDeletionException() {
    }
}