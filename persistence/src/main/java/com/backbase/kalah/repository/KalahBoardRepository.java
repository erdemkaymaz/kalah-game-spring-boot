package com.backbase.kalah.repository;

import com.backbase.kalah.model.KalahBoard;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface KalahBoardRepository extends CrudRepository<KalahBoard, Long> {

}