package com.backbase.kalah.repository;

import com.backbase.kalah.model.Pit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface PitRepository extends CrudRepository<Pit, Long> {

}