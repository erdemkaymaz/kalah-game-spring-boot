package com.backbase.kalah.model;

import javax.persistence.*;

@Entity
@Table(name = "pit")
public class Pit extends BaseGameComponent {

    @Column(name = "pit_number")
    private Integer pitNumber;

    @ManyToOne(fetch = FetchType.EAGER)
    private Player pitOwner;

    @Column(name = "number_of_stones")
    private Integer numberOfStones;

    @ManyToOne(fetch = FetchType.EAGER)
    private KalahBoard board;

    public Integer getPitNumber() {
        return pitNumber;
    }

    public Pit setPitNumber(Integer pitNumber) {
        this.pitNumber = pitNumber;
        return this;
    }

    public Player getPitOwner() {
        return pitOwner;
    }

    public Pit setPitOwner(Player pitOwner) {
        this.pitOwner = pitOwner;
        return this;
    }

    public Integer getNumberOfStones() {
        return numberOfStones;
    }

    public Pit setNumberOfStones(Integer numberOfStones) {
        this.numberOfStones = numberOfStones;
        return this;
    }

    public KalahBoard getBoard() {
        return board;
    }

    public Pit setBoard(KalahBoard board) {
        this.board = board;
        return this;
    }
}
