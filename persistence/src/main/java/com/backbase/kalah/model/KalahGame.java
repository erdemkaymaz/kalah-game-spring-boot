package com.backbase.kalah.model;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "kalah_game")
public class KalahGame extends BaseGameComponent {

    @OneToOne(mappedBy = "kalahGame", cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    private KalahBoard board;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "game")
    private Set<Player> players = new HashSet<Player>();

    @Column(name = "game_status")
    private String gameStatus;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "game_turn")
    private Integer gameTurn;

    public String getGameStatus() {
        return gameStatus;
    }

    public KalahGame setGameStatus(String gameStatus) {
        this.gameStatus = gameStatus;
        return this;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public KalahGame setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public Set<Player> getPlayers() {
        return players;
    }

    public KalahGame setPlayers(Set<Player> players) {
        this.players = players;
        return this;
    }

    public KalahBoard getBoard() {
        return board;
    }

    public KalahGame setBoard(KalahBoard board) {
        this.board = board;
        return this;
    }

    public Integer getGameTurn() {
        return gameTurn;
    }

    public KalahGame setGameTurn(Integer gameTurn) {
        this.gameTurn = gameTurn;
        return this;
    }
}
