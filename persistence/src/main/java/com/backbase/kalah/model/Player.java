package com.backbase.kalah.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "player")
public class Player extends BaseGameComponent {

    @Column(name = "player_number")
    private Integer playerNumber;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "email")
    private String email;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "pitOwner")
    public Set<Pit> pits = new HashSet<Pit>();

    @OneToOne(mappedBy = "kalahStoreOwner", cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    private KalahStore store;

    @ManyToOne(fetch = FetchType.EAGER)
    private KalahGame game;

    public String getName() {
        return name;
    }

    public Player setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public Player setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Player setEmail(String email) {
        this.email = email;
        return this;
    }

    public Set<Pit> getPits() {
        return pits;
    }

    public Player setPits(Set<Pit> pits) {
        this.pits = pits;
        return this;
    }

    public KalahStore getStore() {
        return store;
    }

    public Player setStore(KalahStore store) {
        this.store = store;
        return this;
    }

    public KalahGame getGame() {
        return game;
    }

    public Player setGame(KalahGame game) {
        this.game = game;
        return this;
    }

    public Integer getPlayerNumber() {
        return playerNumber;
    }

    public Player setPlayerNumber(Integer playerNumber) {
        this.playerNumber = playerNumber;
        return this;
    }
}
