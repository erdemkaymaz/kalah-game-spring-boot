package com.backbase.kalah.model;

import javax.persistence.*;

@Entity
@Table(name = "kalah_store")
public class KalahStore extends BaseGameComponent {

    @Column(name = "kalah_store_number")
    private Integer kalahStoreNumber;

    @Column(name = "number_of_stones")
    private Integer numberOfStones;

    @JoinColumn(name = "kalah_store_owner_id")
    @OneToOne(fetch = FetchType.EAGER)
    private Player kalahStoreOwner;

    @ManyToOne(fetch = FetchType.EAGER)
    private KalahBoard board;

    public Integer getKalahStoreNumber() {
        return kalahStoreNumber;
    }

    public KalahStore setKalahStoreNumber(Integer kalahStoreNumber) {
        this.kalahStoreNumber = kalahStoreNumber;
        return this;
    }

    public Integer getNumberOfStones() {
        return numberOfStones;
    }

    public KalahStore setNumberOfStones(Integer numberOfStones) {
        this.numberOfStones = numberOfStones;
        return this;
    }

    public Player getKalahStoreOwner() {
        return kalahStoreOwner;
    }

    public KalahStore setKalahStoreOwner(Player kalahStoreOwner) {
        this.kalahStoreOwner = kalahStoreOwner;
        return this;
    }

    public KalahBoard getBoard() {
        return board;
    }

    public KalahStore setBoard(KalahBoard board) {
        this.board = board;
        return this;
    }
}
