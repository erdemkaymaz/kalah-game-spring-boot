package com.backbase.kalah.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "kalah_board")
public class KalahBoard extends BaseGameComponent {

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "board")
    public List<Pit> pits = new ArrayList<Pit>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "board")
    public List<KalahStore> kalahStores = new ArrayList<KalahStore>();

    @JoinColumn(name = "kalah_game_id")
    @OneToOne(fetch = FetchType.EAGER)
    private KalahGame kalahGame;

    public List<Pit> getPits() {
        return pits;
    }

    public KalahBoard setPits(List<Pit> pits) {
        this.pits = pits;
        return this;
    }

    public List<KalahStore> getKalahStores() {
        return kalahStores;
    }

    public KalahBoard setKalahStores(List<KalahStore> kalahStores) {
        this.kalahStores = kalahStores;
        return this;
    }

    public KalahGame getKalahGame() {
        return kalahGame;
    }

    public KalahBoard setKalahGame(KalahGame kalahGame) {
        this.kalahGame = kalahGame;
        return this;
    }
}
